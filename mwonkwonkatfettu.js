#!/usr/bin/env node
"use strict";

const fs = require('fs');


fs.readFile('pyashWords.json', 'utf8', (error1, pyashWords) => {
	const qrispyactlatpu = JSON.parse(pyashWords);
	let qrissopyacmakwonli = {};
	for (let i = 0; i < qrispyactlatpu.length; i++) {
		qrissopyacmakwonli[qrispyactlatpu[i].en] = qrispyactlatpu[i].pya;
	}

fs.readFile('genTransX.json', 'utf8', (error, contents) => {
		contents = JSON.parse(contents);
		//console.log(contents.zh.pyash_dativeCase);
		const chinese_words = Object.keys(contents).map((translations) => {
			let qristlat = translations.slice(1);
			return {en: qristlat, zh: contents[translations].zh, pya: qrissopyacmakwonli[qristlat]};
		});
		//const sorted_words = chinese_words.sort((first, second) => {
		//	return first.zh.length - second.zh.length || first.zh.localeCompare(second.zh);
		//});
		console.log(JSON.stringify(chinese_words));
});
});
