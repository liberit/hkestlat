#!/usr/bin/env node
"use strict";

const fs = require('fs');
fs.readFile('cedict.json', 'utf8', (error, contents) => {
	const cedict = JSON.parse(contents);
	// fix "Variant of", replace with the actual meaning
	let mwonsoqrismakwonli = {};
	for (let i = 0; i < cedict.length; i++) {
		if (cedict[i].en.match(/variant of/)) continue;
		mwonsoqrismakwonli[cedict[i].zh] = cedict[i].en;
	}
	let psaskwonli = [];
	for (let i = 0; i < cedict.length; i++) {
		let psaspsut = {};
		psaspsut.zh = cedict[i].zh;
		let qrispsas = cedict[i].en;
		if (cedict[i].en.match(/variant of/)) {
			let re = /[\u4e00-\u9fa5]$/ 
			//console.log(cedict[i].en);
			let nyif = re.exec(cedict[i].en);
			if (nyif) {
			qrispsas = mwonsoqrismakwonli[nyif[0]];
			//console.log(nyif[0]+ " " + qrispsas);
			} 
		}
		psaspsut.en = qrispsas;
		psaskwonli.push(psaspsut);
	}
	console.log(JSON.stringify(psaskwonli));
});
