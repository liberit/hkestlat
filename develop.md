### 2021-11-11 What next?
So our goal here is to make pyash compounds based on the chinese words.
In order to do that, first we need to link the existing pyash words, each with a corresponding chinese word. 
We can make word definitions by matching chinese words, and chinese word radicals to their corresponding pyash word. 
