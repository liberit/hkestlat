#!/usr/bin/env node
"use strict";

const fs = require('fs');
const foreign_code = process.argv[2];
fs.readFile('genTransX.json', 'utf8', (error, contents) => {
		contents = JSON.parse(contents);
		const foreign_words = Object.keys(contents).map((translations) => {
			return {en: translations.slice(1), [foreign_code]: contents[translations][foreign_code]};
		});
//		console.log(JSON.stringify(foreign_words));
		const sorted_words = foreign_words.sort((first, second) => {
			if (first[foreign_code] && second[foreign_code]) {
			return first[foreign_code].length - second[foreign_code].length || first[foreign_code].localeCompare(second[foreign_code]);
			} 
		});
		console.log(JSON.stringify(sorted_words));
});
